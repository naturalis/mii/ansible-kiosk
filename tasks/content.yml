---
- name: Set kiosk directory as fact
  set_fact:
    kiosk_path: "{{ interactive_user.home }}/kiosk/{{ kiosk.id }}"
  tags:
    - content

- name: Create kiosk directory
  file:
    state: directory
    name: "{{ kiosk_path }}"
    owner: "{{ interactive_user.name }}"
    group: "{{ interactive_user.name }}"

- name: Create kiosk subdirectories
  file:
    state: directory
    name: "{{ kiosk_path }}/{{ item }}"
    owner: "{{ interactive_user.name }}"
    group: "{{ interactive_user.name }}"
  with_items:
    - zip
    - tmp
    - prd
    - old

- name: Download content from Nextcloud
  command: "/usr/local/bin/nextcloud_update --config {{ interactive_user.home }}/.nextcloud.json {{ kiosk.contentdir | default(ansible_hostname) }}/{{ kiosk.content }}  {{ kiosk_path }}/zip/"
  register: update_content
  become: yes
  become_user: "{{ interactive_user.name }}"
  changed_when: "'not updated' not in update_content.stderr"
  tags:
    - content

- name: Unzip content to temporary directory
  unarchive:
    src: "{{ kiosk_path }}/zip/{{ kiosk.content }}"
    dest: "{{ kiosk_path }}/tmp/"
    owner: "{{ interactive_user.name }}"
    group: "{{ interactive_user.name }}"
    remote_src: yes
  become: yes
  become_user: "{{ interactive_user.name }}"
  when: update_content.changed
  tags:
    - content

- name: Stop systemd kiosk service
  systemd:
    name: kiosk-{{ kiosk.id }}.service
    state: stopped
    scope: user
  become: yes
  become_user: "{{ interactive_user.name }}"
  environment:
    XDG_RUNTIME_DIR: "/run/user/{{ interactive_user.id }}"
    DBUS_SESSION_BUS_ADDRESS: "unix:path=/run/user/{{ interactive_user.id }}/bus"
  when: update_content.changed

- name: Remove old version
  file:
    state: absent
    name: "{{ kiosk_path }}/old"
  when: update_content.changed

- name: Move production to old
  command: 'mv {{ kiosk_path }}/prd {{ kiosk_path }}/old'
  when: update_content.changed

- name: Move new kiosk version to prd directory
  command: 'mv {{ kiosk_path }}/tmp {{ kiosk_path }}/prd'
  when: kiosk.zipdir | length == 0 and update_content.changed

- name: Move new kiosk version to prd directory
  command: 'mv {{ kiosk_path }}/tmp/{{ kiosk.zipdir }} {{ kiosk_path }}/prd'
  when: kiosk.zipdir | length > 0 and update_content.changed

- name: Recreate kiosk subdirectories
  file:
    state: directory
    name: "{{ kiosk_path }}/{{ item }}"
    owner: "{{ interactive_user.name }}"
    group: "{{ interactive_user.name }}"
  with_items:
    - zip
    - tmp
    - prd
    - old
  when: update_content.changed

- name: Start systemd kiosk service
  systemd:
    name: kiosk-{{ kiosk.id }}
    state: started
    scope: user
  become_user: "{{ interactive_user.name }}"
  environment:
    XDG_RUNTIME_DIR: "/run/user/{{ interactive_user.id }}"
    DBUS_SESSION_BUS_ADDRESS: "unix:path=/run/user/{{ interactive_user.id }}/bus"
